function defaults(obj, defaultProps) {
    if (!obj){
        return defaultProps;
    }
    for (let key in defaultProps) {
        if (!(key in obj)) {
            obj[key] = defaultProps[key]
        }
    }
    return obj
}

module.exports = defaults;