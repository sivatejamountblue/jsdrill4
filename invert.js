function invert(obj){
    if (!obj){
        return {};
    }
    let object = {};
    for(let key in obj){
    object[obj[key]] = key;
    }
    return object;
}
module.exports = invert;