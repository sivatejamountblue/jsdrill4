function mapObject(obj, cb) {
    if (!obj){
        return {};
    }
    let object = {}
    for (let each in obj){
        let result = (cb(obj[each], each))
        object[each] = result;
    }
    return object
}
module.exports = mapObject;