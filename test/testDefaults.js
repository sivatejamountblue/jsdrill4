const defaults = require("../defaults")
const testObject = { name: 'Wayne', age: 36, location: 'Gotham' };
const defaultProp = {name: 'Bruce Wayne', age: 36, location: 'Gotham', gender: 'Male'};
let result = defaults(testObject, defaultProp);
console.log(result);